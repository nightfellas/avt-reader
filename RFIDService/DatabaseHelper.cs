﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace RFIDService
{
    class DatabaseHelper
    {

        string extension = ".sql";
        
        string root = Path.GetDirectoryName(Application.ExecutablePath) + "\\SQL\\";
        string PROCESS_ID;
        LogWriter log = new LogWriter();
        public void updateConnection(string ip_address, string status)
        {
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            string connectionString = config.AppSettings.Settings["connectionString"].Value;
            var path = Path.Combine(root, "updateConnection" + extension);
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            string query = file.ReadToEnd();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = query;
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@ip_address", ip_address);
                command.Parameters.AddWithValue("@status", status);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        //BODY_NO = String.Format("{0}",reader["BODY_NO"]);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
        }


        public BodyData checkBodyNo(string body_no)
        {
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            string connectionString = config.AppSettings.Settings["connectionString"].Value;
            BodyData b = new BodyData();
            //string BODY_NO = "";
            var path = Path.Combine(root, "checkBodyNo" + extension);
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            string query = file.ReadToEnd();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = query;
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@BODY_NO", body_no);
                //command.Parameters.AddWithValue("@POST_SEQ", post_seq);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        BodyData c = new BodyData();
                        c.BODY_NO = String.Format("{0}", reader["BODY_NO"]);
                        c.POST_SEQ = String.Format("{0}", reader["POST_SEQ"]);
                        c.SEQ_NO = String.Format("{0}", reader["SEQ_NO"]);
                        c.VINNO = String.Format("{0}", reader["VINNO"]);
                        b = c;
                    }
                }
                catch(Exception ex)
                {
                    log.Write("checkBodyNo : " + ex.Message);
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            
            return b;
           /* if (BODY_NO == body_no)
            {
                return true;
            }
            else
            {
                return false;
            }
            */
        }
        /*
        public bool checkBodyNo(string body_no, string post_seq)
        {
            string BODY_NO = "";
            var path = Path.Combine(root, "checkBodyNo" + extension);
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            string query = file.ReadToEnd();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = query;
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@BODY_NO", body_no);
                command.Parameters.AddWithValue("@POST_SEQ", post_seq);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        BODY_NO = String.Format("{0}", reader["BODY_NO"]);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            if (BODY_NO == body_no)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        */

        public bool insert_TB_R_RFID_READ(string rguid, string post_id, string body_no, string vinno, string epc_id, string antena_id, string ip_address, string post_seq, string events, string RSSI, string pcbits, string memoryBankData, string memoryBank, string offset)
        {
            log.Write("function insert_TB_R_RFID_READ");
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            string connectionString = config.AppSettings.Settings["connectionString"].Value;
            //log.Write(connectionString);
            string enablePost = null;
            var path = Path.Combine(root, "insert_TB_R_RFID_READ" + extension);
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            string query = file.ReadToEnd();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = query;
                //log.Write(queryString);
                SqlCommand command = new SqlCommand(queryString, connection);
                /*
                log.Write("@rguid : " + rguid);
                log.Write("@post_id : " + post_id);
                log.Write("@body_no : " + body_no);
                log.Write("@epc_id : " + epc_id);
                log.Write("@antena_id : " + antena_id);
                log.Write("@vinno : " + vinno);
                log.Write("@ip_address : " + ip_address);
                log.Write("@post_seq : " + post_seq);

                log.Write("@event : " + events);
                log.Write("@RSSI : " + RSSI);
                log.Write("@pcbits : " + pcbits);
                log.Write("@memoryBankData : " + memoryBankData);
                log.Write("@memoryBank : " + memoryBank);
                log.Write("@offset : " + offset);
                */
                command.Parameters.AddWithValue("@rguid", rguid);
                command.Parameters.AddWithValue("@post_id", post_id);
                command.Parameters.AddWithValue("@body_no", body_no);
                command.Parameters.AddWithValue("@epc_id", epc_id);
                command.Parameters.AddWithValue("@antena_id", antena_id);
                command.Parameters.AddWithValue("@vinno", vinno);
                command.Parameters.AddWithValue("@ip_address", ip_address);
                command.Parameters.AddWithValue("@post_seq", post_seq);

                command.Parameters.AddWithValue("@event", events);
                command.Parameters.AddWithValue("@RSSI", RSSI);
                command.Parameters.AddWithValue("@pcbits", pcbits);
                command.Parameters.AddWithValue("@memoryBankData", memoryBankData);
                command.Parameters.AddWithValue("@memoryBank", memoryBank);
                command.Parameters.AddWithValue("@offset", offset);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        //BODY_NO = String.Format("{0}",reader["BODY_NO"]);
                        enablePost = String.Format("{0}", reader["enablePost"]);
                        log.Write("Enable Post Write Cursor : " + enablePost);
                    }
                }
                catch(Exception ex)
                {
                    log.Write(ex.Message);
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            //log.Write("enabelPost: " + enablePost);
            if (enablePost == "1")
            {
                return true;
            }
            else
            {
                return false;
            }

            
        }


        public bool InsertBodyNumber(string body_number, string post_id, string post_seq)
        {
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            string connectionString = config.AppSettings.Settings["connectionString"].Value;
            var path = Path.Combine(root, "insertBodyNumber" + extension);
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            string query = file.ReadToEnd();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = query;
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@POST_ID", post_id);
                command.Parameters.AddWithValue("@BODY_NO", body_number);
                command.Parameters.AddWithValue("@POST_SEQ", post_seq);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

            }
            return true;
        }

        public string putLog(string function_id, string process_id, string process_status, string location, string msg_id, string message)
        {
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            string connectionString = config.AppSettings.Settings["connectionString"].Value;
            var path = Path.Combine(root, "putLog" + extension);
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            string query = file.ReadToEnd();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = query;
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@FUNCTION_ID", function_id);
                command.Parameters.AddWithValue("@PROCESS_ID", process_id);
                command.Parameters.AddWithValue("@PROCESS_STATUS", process_status);
                command.Parameters.AddWithValue("@LOCATION", location);
                command.Parameters.AddWithValue("@MSG_ID", msg_id);
                command.Parameters.AddWithValue("@MESSAGE", message);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        PROCESS_ID = String.Format("{0}", reader["PROCESS_ID"]);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    reader.Close();
                }
            }
            return PROCESS_ID;
        }
    }
}
