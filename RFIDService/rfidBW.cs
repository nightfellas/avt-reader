﻿using Symbol.RFID3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Text;
using System.Windows.Forms;

namespace RFIDService
{
    class rfidBW
    {
        internal RFIDReader m_ReaderAPI;
        internal bool m_IsConnected;
        private BackgroundWorker connectBackgroundWorker = new BackgroundWorker();
        private uint m_TagTotalCount;
        private TagData m_ReadTag = null;
        private delegate void UpdateRead(Events.ReadEventData eventData);
        private UpdateRead m_UpdateReadHandler = null;
        private Hashtable m_TagTable;
        private ArrayList TAGS;
        private ArrayList BN;
        private ArrayList ANT;
        private string BODY_NO = "";
        private string mess = "";
        public string ip_address;
        public string post_id;
        public string post_seq;
        private Symbol.RFID3.TriggerInfo m_TriggerInfo = null;
        private DatabaseHelper db = new DatabaseHelper();
        public LogWriter log = new LogWriter();
        //public string PROCESS_ID;

        public rfidBW(string command, string ip_address, string post_id, string process_id, string post_seq)
        {
            this.ip_address = ip_address;
            this.post_id = post_id;
            this.post_seq = post_seq;
            //this.PROCESS_ID = process_id;
            connectBackgroundWorker.DoWork += connectBackgroundWorker_DoWork;
            connectBackgroundWorker.ProgressChanged += connectBackgroundWorker_ProgressChanged;
            connectBackgroundWorker.RunWorkerCompleted += connectBackgroundWorker_RunWorkerCompleted;
            connectBackgroundWorker.WorkerSupportsCancellation = true;

            m_UpdateReadHandler = new UpdateRead(myUpdateRead);
            m_TagTable = new Hashtable();
            TAGS = new ArrayList();
            BN = new ArrayList();
            ANT = new ArrayList();
            log.Write("rfidBW has been called for IP : " + ip_address);
            connectBackgroundWorker.RunWorkerAsync("Connect");
        }

        public void connectBackgroundWorker_DoWork(object sender, DoWorkEventArgs workEventArgs)
        {
            //log.Write("connectBackgroundWorker_DoWork has been called for IP : " + ip_address);
            if ((string)workEventArgs.Argument == "Connect")
            {
                m_ReaderAPI = new RFIDReader(ip_address, 0, 0);

                try
                {
                    m_ReaderAPI.Connect();
                    m_IsConnected = true;
                    TagAccess.Sequence.Operation op = new TagAccess.Sequence.Operation();
                    op.AccessOperationCode = ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ;
                    op.ReadAccessParams.MemoryBank = (MEMORY_BANK)3;
                    op.ReadAccessParams.ByteCount = 0;
                    op.ReadAccessParams.ByteOffset = 0;
                    m_ReaderAPI.Actions.TagAccess.OperationSequence.Add(op);

                    workEventArgs.Result = "Connect Succeed";
                    //log.Write("connectBackgroundWorker_DoWork : Connect Succeed for ip " + ip_address);
                }
                catch (OperationFailureException operationException)
                {
                    workEventArgs.Result = operationException.Result;
                    //log.Write("connectBackgroundWorker_DoWork : " + operationException.Result + " for ip " + ip_address);

                }
                catch (Exception ex)
                {
                    workEventArgs.Result = ex.Message;
                    //log.Write("connectBackgroundWorker_DoWork : " + ex.Message + " for ip " + ip_address);
                }
            }
            else if ((string)workEventArgs.Argument == "Disconnect")
            {
                try
                {
                    m_ReaderAPI.Disconnect();
                    m_IsConnected = false;
                    workEventArgs.Result = "Disconnect Succeed";
                    //log.Write("connectBackgroundWorker_DoWork : Disconnect Succeed for ip " + ip_address);
                }
                catch (OperationFailureException ofe)
                {
                    workEventArgs.Result = ofe.Result;
                    //log.Write("connectBackgroundWorker_DoWork : " + ofe.Result + " for ip " + ip_address);
                }
            }
        }

        public void connectBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs progressEventArgs)
        {
            Console.WriteLine(progressEventArgs);
            //log.Write("connectBackgroundWorker_ProgressChanged : " + progressEventArgs + " for ip " + ip_address);
        }


        public void connectBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs connectEventArgs)
        {
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            int signal_gain = Convert.ToInt32(config.AppSettings.Settings["signal_gain"].Value);
            
            //Console.WriteLine(connectEventArgs.Result.ToString());
            //log.Write("connectBackgroundWorker_RunWorkerCompleted : " + connectEventArgs.Result.ToString() + " for ip " + ip_address);
            if (connectEventArgs.Result.ToString() == "Connect Succeed")
            {
                ushort[] antID = m_ReaderAPI.Config.Antennas.AvailableAntennas;
                foreach (ushort i in antID)
                {
                    int gain = (signal_gain - 1000) / 10;
                    //Console.WriteLine(i.ToString());
                    Antennas.Config antConfig = m_ReaderAPI.Config.Antennas[i].GetConfig();
                    
                    antConfig.ReceiveSensitivityIndex = 0;
                    antConfig.TransmitPowerIndex = (ushort)gain;
                    antConfig.TransmitFrequencyIndex = 1;
                    m_ReaderAPI.Config.Antennas[i].SetConfig(antConfig);
                }

                ////////("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Device : " + ip_address + " is connected");
                //Console.WriteLine("Connected to : " + ip_address);
                m_ReaderAPI.Actions.PreFilters.DeleteAll();
                m_ReaderAPI.Events.ReadNotify += new Events.ReadNotifyHandler(Events_ReadNotify);
                m_ReaderAPI.Events.NotifyGPIEvent = true;
                m_ReaderAPI.Events.NotifyAntennaEvent = true;
                m_ReaderAPI.Events.NotifyReaderDisconnectEvent = true;
                m_ReaderAPI.Events.NotifyBufferFullEvent = true;
                m_ReaderAPI.Events.NotifyBufferFullWarningEvent = true;
                m_ReaderAPI.Events.NotifyAccessStartEvent = true;
                m_ReaderAPI.Events.NotifyAccessStopEvent = true;
                m_ReaderAPI.Events.NotifyInventoryStartEvent = true;
                m_ReaderAPI.Events.NotifyInventoryStopEvent = true;
                m_ReaderAPI.Events.NotifyReaderExceptionEvent = true;
                ////////("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Start Reading on device : "+ip_address);
                startReading();
            }
            else
            {
                //Console.WriteLine("Disconnected from : " + ip_address);
                ////////("113", PROCESS_ID, "3", "RFID Reader Service", "ERR", "Device : " + ip_address + " is failed to connect");
            }
        }

        public void Events_ReadNotify(object sender, Events.ReadEventArgs readEventArgs)
        {
            try
            {
                myUpdateRead(readEventArgs.ReadEventData);
            }
            catch (Exception ex)
            {
                ////////("113", PROCESS_ID, "3", "RFID Reader Service", "ERR", "Reading Error : " + ex.Message);
                //log.Write("EVENTS_READNOTIFY : " + ex.Message);
            }
        }
        
        /*private void myUpdateRead(Events.ReadEventData eventData)
        {
            Symbol.RFID3.TagData[] tagData = m_ReaderAPI.Actions.GetReadTags(1000);
            if (tagData != null)
            {
                for (int nIndex = 0; nIndex < tagData.Length; nIndex++)
                {
                    if (tagData[nIndex].OpCode == ACCESS_OPERATION_CODE.ACCESS_OPERATION_NONE ||
                        (tagData[nIndex].OpCode == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ &&
                        tagData[nIndex].OpStatus == ACCESS_OPERATION_STATUS.ACCESS_SUCCESS))
                    {
                        Symbol.RFID3.TagData tag = tagData[nIndex];
                        string tagID = tag.TagID;
                        bool isFound = false;

                        lock (m_TagTable.SyncRoot)
                        {
                            isFound = m_TagTable.ContainsKey(tagID);
                            if (!isFound)
                            {
                                tagID = tag.TagID + tag.MemoryBank.ToString()
                                    + tag.MemoryBankDataOffset.ToString();
                                isFound = m_TagTable.ContainsKey(tagID);
                            }
                        }
                        log.Write("TAG SEEN : "+Convert.ToString(tag.TagSeenCount));
                        if (isFound)
                        {
                            uint count = 0;
                            ListViewItem item = (ListViewItem)m_TagTable[tagID];
                            try
                            {
                                count = uint.Parse(item.SubItems[3].Text) + tagData[nIndex].TagSeenCount;
                                m_TagTotalCount += tagData[nIndex].TagSeenCount;
                            }
                            catch (FormatException fe)
                            {
                                break;
                            }
                            item.SubItems[2].Text = tag.AntennaID.ToString();
                            item.SubItems[3].Text = count.ToString();
                            item.SubItems[4].Text = tag.PeakRSSI.ToString();

                            string memoryBank = tag.MemoryBank.ToString();
                            int index = memoryBank.LastIndexOf('_');
                            if (index != -1)
                            {
                                memoryBank = memoryBank.Substring(index + 1);
                            }
                            if (tag.MemoryBankData.Length > 0 && !memoryBank.Equals(item.SubItems[5].Text))
                            {
                                item.SubItems[6].Text = tag.MemoryBankData.Substring(0, 44);
                                item.SubItems[7].Text = memoryBank;
                                item.SubItems[8].Text = tag.MemoryBankDataOffset.ToString();

                                lock (m_TagTable.SyncRoot)
                                {
                                    m_TagTable.Remove(tagID);
                                    m_TagTable.Add(tag.TagID + tag.MemoryBank.ToString()
                                        + tag.MemoryBankDataOffset.ToString(), item);
                                }
                            }
                            item.SubItems[1].Text = getTagEvent(tag);
                            if (getTagEvent(tag) == "Gone")
                            {
                                int i = TAGS.IndexOf(tag.TagID);
                                Console.WriteLine("gone i : " + i.ToString());
                                if (i > -1)
                                {
                                    TAGS.RemoveAt(i);
                                }

                            }
                            else
                            {
                                int i = TAGS.IndexOf(tag.TagID);
                                Console.WriteLine("gone i : " + i.ToString());
                                if (i > -1)
                                {
                                    TAGS.RemoveAt(i);
                                    TAGS.Add(tag.TagID);
                                }
                                else
                                {
                                    TAGS.Add(tag.TagID);
                                }
                            }
                        }
                        else
                        {
                            int i = TAGS.IndexOf(tag.TagID);
                            Console.WriteLine("not found i : " + i.ToString());
                            if (i == -1)
                            {
                                TAGS.Add(tag.TagID);
                            }
                            ListViewItem item = new ListViewItem(tag.TagID);
                            ListViewItem.ListViewSubItem subItem;

                            subItem = new ListViewItem.ListViewSubItem(item, getTagEvent(tag));
                            item.SubItems.Add(subItem);

                            subItem = new ListViewItem.ListViewSubItem(item, tag.AntennaID.ToString());
                            item.SubItems.Add(subItem);

                            subItem = new ListViewItem.ListViewSubItem(item, tag.TagSeenCount.ToString());
                            m_TagTotalCount += tag.TagSeenCount;
                            item.SubItems.Add(subItem);

                            subItem = new ListViewItem.ListViewSubItem(item, tag.PeakRSSI.ToString());
                            item.SubItems.Add(subItem);

                            subItem = new ListViewItem.ListViewSubItem(item, tag.PC.ToString("X"));
                            item.SubItems.Add(subItem);

                            lock (m_TagTable.SyncRoot)
                            {
                                m_TagTable.Add(tagID, item);
                            }
                        }
                    }
                }
            }

            log.Write("COUNT : " + Convert.ToString(TAGS.Count));
            if (TAGS.Count == 1)
            {
                log.Write("BODY NO : " + HEX2ASCII(TAGS[0].ToString()));
            }
            else if (TAGS.Count > 1)
            {
                log.Write("More than one tag detected");
            }
            else
            {
                log.Write("No tag detected");
            }
        }
       */
       
        public void myUpdateRead(Events.ReadEventData eventData)
        {
            
            string tag_id = eventData.TagData.TagID;
            //string body_no = HEX2ASCII(eventData.TagData.MemoryBankData.Substring(0, 44));
            string body_no = HEX2ASCII(eventData.TagData.TagID);
            //log.Write("body_no : " + body_no);
            string antena_id = eventData.TagData.AntennaID.ToString();
            string event_occure = getTagEvent(eventData.TagData);
            string RSSI = eventData.TagData.PeakRSSI.ToString();
            string memoryBank = eventData.TagData.MemoryBank.ToString();
            int index = memoryBank.LastIndexOf('_');
            if (index != -1)
            {
                memoryBank = memoryBank.Substring(index + 1);
            }
            string memoryBankData = eventData.TagData.MemoryBankData.ToString();
            string offset = eventData.TagData.MemoryBankDataOffset.ToString();
            string pcbits = eventData.TagData.PC.ToString();
            if(event_occure!="Gone")
            {
                int i = TAGS.IndexOf(tag_id);
                Console.WriteLine("gone i : " + i.ToString());
                if (i > -1)
                {
                    TAGS.RemoveAt(i);
                    TAGS.Add(tag_id);
                }
                else
                {
                    TAGS.Add(tag_id);
                }
            }
            else
            {
                int i = TAGS.IndexOf(tag_id);
                Console.WriteLine("gone i : " + i.ToString());
                if (i > -1)
                {
                    TAGS.RemoveAt(i);
                }
            }

            if(TAGS.Count==1)
            {
                string PROCESS_ID;
                PROCESS_ID = db.putLog("113", "", "1", "RFID Reader Service", "INF", "Reading Tag");
                BodyData b = db.checkBodyNo(body_no);
                //log.Write("b.BODY_NO : " + b.BODY_NO);
                if (b.BODY_NO != null)
                {
                    Console.WriteLine("The body number exist");
                    db.putLog("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Body number : " + body_no + " is exists");
                    db.putLog("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "INSERT INTO TB_R_RFID_READ for Body Number : " + body_no);
                    //log.Write("INSERT INTO TB_R_RFID_READ for Body Number : " + body_no);
                    string rguid = DateTime.Now.ToString("yyyyMMddHmmssfffffff") + RandomString();
                    if (db.insert_TB_R_RFID_READ(rguid, post_id, b.BODY_NO, b.VINNO, tag_id, antena_id, ip_address, post_seq, event_occure, RSSI, pcbits, memoryBankData, memoryBank, offset))
                    {
                        db.putLog("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "The body number successfully inserted and will directly send to ALC by copy and paste");

                        //Console.WriteLine("Copy paste");
                        log.Write("Copy Paste");
                        db.putLog("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Connecting to socket");
                        SocketHelper s = new SocketHelper("");
                        s.sendToSocket(body_no, "");
                        s.closeSocket("");
                    }
                    else
                    {
                        db.putLog("113", PROCESS_ID, "2", "RFID Reader Service", "INF", "The body number successfully inserted and will directly send to ALC by ALC SENDER");
                        //Console.WriteLine("Not Copy Paste");
                        log.Write("Not Copy Paste");
                        db.putLog("113", PROCESS_ID, "2", "RFID Reader Service", "INF", "Process finished successfuly");
                    }
                }
                else
                {
                    db.putLog("113", PROCESS_ID, "3", "RFID Reader Service", "INF", "Body number is not exist");
                    db.putLog("113", PROCESS_ID, "3", "RFID Reader Service", "INF", "Process finished with errors");
                    log.Write("Body Number not exist");
                }
            }
            else if(TAGS.Count>1)
            {
                log.Write("more than one tag detected, only one tag will be send to the socket receiver");
            }
            else{
                log.Write("No tag detected");
            }
        }
        

        public string HEX2ASCII(string hex)
        {
            string res = String.Empty;
            for (int a = 0; a < hex.Length; a = a + 2)
            {
                string Char2Convert = hex.Substring(a, 2);
                int n = Convert.ToInt32(Char2Convert, 16);
                char c = (char)n;
                res += c.ToString();
            }
            return res;
        }

        public void startReading()
        {
            ////////("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Start reading on device : " + ip_address);
            log.Write("startReading");
            if (m_ReaderAPI.Actions.TagAccess.OperationSequence.Length > 0)
            {
                m_ReaderAPI.Actions.TagAccess.OperationSequence.PerformSequence(null, getTriggerInfo(), null);
                //m_ReaderAPI.Actions.TagAccess.OperationSequence.PerformSequence(null, null, null);
            }
            else
            {
                m_TagTable.Clear();
                m_TagTotalCount = 0;
                m_ReaderAPI.Actions.Inventory.Perform(null, getTriggerInfo(), null);
                //m_ReaderAPI.Actions.Inventory.Perform(null, null, null);
            }
        }

        public string getTagEvent(TagData tag)
        {
            string eventString = "None";
            if (tag.TagEvent != TAG_EVENT.NONE)
            {
                switch (tag.TagEvent)
                {
                    case TAG_EVENT.NEW_TAG_VISIBLE:
                        eventString = "New";
                        break;
                    case TAG_EVENT.TAG_BACK_TO_VISIBILITY:
                        eventString = "Back";
                        break;
                    case TAG_EVENT.TAG_NOT_VISIBLE:
                        eventString = "Gone";
                        break;
                    default:
                        eventString = "None";
                        break;

                }

            }
            //log.Write("getTagEvent : " + tag.MemoryBankData.Substring(0, 5) + " : " + eventString);
            return eventString;
        }

        public Symbol.RFID3.TriggerInfo getTriggerInfo()
        {
            m_TriggerInfo = new TriggerInfo();
            m_TriggerInfo.StartTrigger.Type = START_TRIGGER_TYPE.START_TRIGGER_TYPE_IMMEDIATE;
            m_TriggerInfo.StopTrigger.Type = STOP_TRIGGER_TYPE.STOP_TRIGGER_TYPE_IMMEDIATE;
            m_TriggerInfo.TagEventReportInfo.ReportNewTagEvent = (TAG_EVENT_REPORT_TRIGGER)2;
            m_TriggerInfo.TagEventReportInfo.ReportTagBackToVisibilityEvent = (TAG_EVENT_REPORT_TRIGGER)2;
            m_TriggerInfo.TagEventReportInfo.ReportTagInvisibleEvent = (TAG_EVENT_REPORT_TRIGGER)2;
            m_TriggerInfo.TagEventReportInfo.NewTagEventModeratedTimeoutMilliseconds = ushort.Parse("500");
            m_TriggerInfo.TagEventReportInfo.TagBackToVisibilityModeratedTimeoutMilliseconds = ushort.Parse("500");
            m_TriggerInfo.TagEventReportInfo.TagInvisibleEventModeratedTimeoutMilliseconds = ushort.Parse("500");
            m_TriggerInfo.TagReportTrigger = 1;
            m_TriggerInfo.EnableTagEventReport = true;
            return m_TriggerInfo;
        }

        public static string RandomString()
        {
            Random rnd = new Random();
            //const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string[] alphabet = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            int len = 5;
            int rndCap;
            int rndLetter;
            string answer = "";
            for (int i = 1; i <= len; i++)
            {
                rndCap = rnd.Next(1, 3);
                rndLetter = rnd.Next(1, 27);
                string tempMem = alphabet[rndLetter];

                if (rndCap == 2)
                {
                    tempMem.ToUpper();
                }
                answer = answer + tempMem;
            }
            return answer;
        }
    }
}
