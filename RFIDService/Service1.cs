﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace RFIDService
{
    public partial class Service1 : ServiceBase
    {
        private Thread myNewThread;
        private Dictionary<string, Thread> threadDictionary;
        public LogWriter log = new LogWriter();
        private DatabaseHelper db = new DatabaseHelper();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            log.Write("RFIDService Started");
            start_thread();
        }

        protected override void OnStop()
        {
            stop_service();
            log.Write("RFIDService Stopped");
        }

        public void start_thread()
        {
            
            List<RFIDData> l = new List<RFIDData>();
            threadDictionary = new Dictionary<string, Thread>();
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            string post_id = config.AppSettings.Settings["post_id"].Value;
            string connectionString = config.AppSettings.Settings["connectionString"].Value;
            log.Write(connectionString);
            log.Write("Starting Thread");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = @"SELECT a.POST_ID, a.READER_IP, a.CONN_FLG, b.POST_SEQ FROM TB_M_RFID a LEFT JOIN TB_M_POST b ON b.POST_ID = a.POST_ID WHERE a.POST_ID = '"+post_id+"'";
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        if(reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                RFIDData data = new RFIDData();
                                data.POST_ID = String.Format("{0}", reader["POST_ID"]);
                                data.READER_IP = String.Format("{0}", reader["READER_IP"]);
                                data.STATUS = String.Format("{0}", reader["CONN_FLG"]);
                                data.POST_SEQ = String.Format("{0}", reader["POST_SEQ"]);
                                l.Add(data);
                            }
                        }
                        else
                        {
                            log.Write("Post ID '"+post_id+"' is not registered");
                        }
                    }
                    catch (Exception e)
                    {
                        log.Write("start_thread() : " + e.Message);
                    }
                    finally
                    {
                        reader.Close();
                    }

                    foreach (RFIDData d in l)
                    {

                        string ip_address = d.READER_IP;
                        try
                        {
                            log.Write("CALLING THREADS");
                            myNewThread = new Thread(() => CallToChildThread("start", ip_address, d.STATUS, d.POST_ID, d.POST_SEQ));
                            if (myNewThread.IsAlive)
                            {
                                myNewThread.Abort();
                            }

                            myNewThread.Name = d.POST_ID + d.READER_IP;
                            myNewThread.Start();
                            threadDictionary.Add(d.POST_ID + d.READER_IP, myNewThread);
                            Thread.Sleep(1000);
                        }
                        catch(Exception ec)
                        {
                            log.Write(ec.Message);
                        }
                    }
                }

                //log.Write("Thread Started");
            }
            catch(Exception ex)
            {
                log.Write("1. : "+ex.Message);
                this.Stop();
            }
            
        }

        public void stop_service()
        {

            //List<RFIDData> l = new List<RFIDData>();
            threadDictionary = new Dictionary<string, Thread>();
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            string connectionString = config.AppSettings.Settings["connectionString"].Value;
            
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = @"SELECT a.POST_ID, a.READER_IP, a.CONN_FLG, b.POST_SEQ FROM TB_M_RFID a LEFT JOIN TB_M_POST b ON b.POST_ID = a.POST_ID";
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            RFIDData data = new RFIDData();
                            data.POST_ID = String.Format("{0}", reader["POST_ID"]);
                            data.READER_IP = String.Format("{0}", reader["READER_IP"]);
                            data.STATUS = String.Format("{0}", reader["CONN_FLG"]);
                            db.updateConnection(data.READER_IP, "0");
                           // l.Add(data);
                        }
                    }
                    catch (Exception e)
                    {
                        log.Write(e.Message);
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Write(ex.Message);
            }
        }

        public void CallToChildThread(string command, string ip_address, string status, string post_id, string post_seq)
        {
            //log.Write("CALLING PINGBW");
            PingBW pb = new PingBW(command, ip_address, post_id, post_seq);
        }
    }
}
